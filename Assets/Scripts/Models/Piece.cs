﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece {

	public int row {get; protected set;}		// the row this piece belongs in when complete
	public int col {get; protected set;}		// the col this piece belongs in when complete

	public int rotation = 0;		// default to not rotated away from correct position
	public bool placed = false;		// is the piece correctly placed?

	public Side[] sides;		// array of sides 0 = right, 1 = up, 2 = left, 3 = down

	public Piece (int col, int row) {

		this.row = row;
		this.col = col;

		sides = new Side[4];

		SelectSides ();

		// create a new piece, setup sides array and populate


	}


	void SelectSides () {


		Side side;		// current side being created
		Piece neighbour;		// current neighbour being assessed


		// Right side (choose random socket/plug, unless on last piece in row);

		if (col == PuzzleController.Instance.cols - 1) {	// no neighbour to right of last piece in row

			side = new Side(0,false);
			//Debug.Log ("End of row....");
		
		}

		else {

			// decide on a plug or socket and the type

			side = new Side (GetRandomConnectorID(),DecideIfPlugOrSocket());


		}

		sides [0] = side;	// set right side of this piece to this side


		// Top side (choose random socket/plug, unless on last piece in row);


		if (row == PuzzleController.Instance.rows - 1) {	// no neighbour will be above last piece in col

			side = new Side(0,false);
			//Debug.Log ("End of column....");
		}

		else {

			// decide on a plug or socket and the type

			side = new Side (GetRandomConnectorID(),DecideIfPlugOrSocket());

		}

		sides [1] = side;	// set right side of this piece to this side



		// Left side (always opposite of piece to the left)

		neighbour = GetNeighbour(-1,0);

		if (neighbour == null) {	// no neighbour to left of tile 0

			side = new Side(0,false);
			//Debug.Log ("No neighbour to left...");
		}

		else {

			// get opposite of the tile to the left 
			side = new Side (neighbour.sides [0].connectorID, !neighbour.sides [0].isSocket);

		}

		sides [2] = side;


		// Bottom side (always opposite of piece underneath)

		neighbour = GetNeighbour(0,-1);

		if (neighbour == null) {	// no neighbour to left of tile 0

			//Debug.Log ("No neighbour below...");
			side = new Side(0,false);
		}

		else {

			// get opposite of the tile underneath
			side = new Side (neighbour.sides [1].connectorID, !neighbour.sides [1].isSocket);

		}

		sides [3] = side;

		for (int i = 0; i < 4; i++) {

			Side s = sides [i];

			//Debug.Log ("Piece COL: " + col + " ROW: " + row + " SIDE: " + i + " - " + s.connectorID + "/" + s.isSocket);


		}

	


	}



	public Piece GetNeighbour (int xOffset, int yOffset) {

		if (col + xOffset < 0 || col + xOffset > PuzzleController.Instance.cols - 1) {
			return null;
		}

		if (row + yOffset < 0 || row + yOffset > PuzzleController.Instance.rows - 1) {
			return null;
		}

		// get the neighbouring piece asked for. Return null if out of bounds.

		return PuzzleController.Instance.pieceGrid [col + xOffset,row + yOffset];

	}


	int GetRandomConnectorID () {

		// get a random connector type from the available types

		return Random.Range (0, PieceController.Instance.plugs.Count) + 1;

	}

	bool DecideIfPlugOrSocket () {

		bool isSocket = true;

		if (Random.Range (0, 2) == 1) {
			isSocket = false;
		}

		return isSocket;

		// randomly decide if this piece will slot into or be slotted into on this side
	}


}
