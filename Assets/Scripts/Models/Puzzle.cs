﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle {

	public int rows { get; protected set;}		// how many rows in this puzzle
	public int cols { get; protected set;}		// how many cols in this puzzle
	public int imageID {get; protected set;}	// the imageID for this puzzle (currently un-used)

	public bool complete = false;		// whether this puzzle is complete (currently un-used)
	public float timeTaken = 0f;		// how long the player has taken so far (currently un-used)


	public Puzzle (int cols, int rows, int imageID) {

		this.rows = rows;
		this.cols = cols;
		this.imageID = imageID;

		createPieces (cols, rows);

		// create a new puzzle object, and call function to create its pieces

	}


	void createPieces (int cols, int rows) {

		// loop through the required rows and columns

		for (int i = 0; i < rows; i++) {

			for (int j = 0; j < cols; j++) {

				Piece p = new Piece (j, i);		// create a new piece object
			
				PuzzleController.Instance.pieceGrid [j, i] = p;	// put the piece in the 2D array
				PieceController.Instance.CreatePieceObject (p);		// get controller to create a gameObject

			}
		}
			

	}




}
