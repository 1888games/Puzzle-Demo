﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Side {

	public int connectorID;	// which plug/socket will be used on this side
	public bool isSocket;	// whether this side will slot into another piece or be slotted into

	public Side (int connectorID, bool isSocket) {

		this.connectorID = connectorID;
		this.isSocket = isSocket;

		// create a side object

	}

}
