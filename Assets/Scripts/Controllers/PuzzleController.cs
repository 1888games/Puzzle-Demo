﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleController : MonoBehaviourSingleton<PuzzleController> {

	public int rows {get; protected set;}
	public int cols {get; protected set;}

	public Piece[,] pieceGrid;
	//public Dictionary<Piece, GameObject> pieceToGameObjectMap;


	public Puzzle currentPuzzle;


	// Use this for initialization
	void Start () {

		//pieces = new List<Piece> ();
		//pieceToGameObjectMap = new Dictionary<Piece, GameObject> ();
	

		createNewPuzzle (cols, rows);	// create a brand new puzzle
	}



	void createNewPuzzle(int colsIn, int rowsIn) {

		rows = Random.Range(2,17);
		cols = Random.Range (Mathf.Max(2,rows-2),rows+2);	
		// randomly choose the size. Will become user-definable or depend on difficulty level.
		
		pieceGrid = new Piece[cols, rows];		// setup a new 2D array of the correct size
	
		Puzzle p = new Puzzle (cols, rows, 1);	// create a new puzzle object
		currentPuzzle = p;		// store the puzzle in case we need it later

	}



	public void deletePuzzle() {	

		// called by UI button
		// clean-up an old puzzle (needs checking for memory leaks)

		foreach(Transform child in PieceController.Instance.transform)
		{
			Destroy (child.gameObject);
			// loop through the piece gameObjects and destroy them
		}	

		pieceGrid = null;
		currentPuzzle = null;	// set the grid and puzzle objects to nil

		createNewPuzzle (cols, rows);	// create a new puzzle
			
	}

}
