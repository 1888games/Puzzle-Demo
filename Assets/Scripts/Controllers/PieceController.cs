﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceController : MonoBehaviourSingleton <PieceController> {

	// Responsible for generating a gameObject based on a piece design.
	// Also holds mask data


	public Dictionary<int, Texture2D> plugs;		// holds any number of 'plug' masks, along with their IDs
	public Dictionary<int, Texture2D> sockets;		// holds any number of 'socker' masks, along with their IDs
	Texture2D edge;		// holds the edge mask, used when a piece is at the edge of the puzzle

	public GameObject piecePrefab;	// holds the prototype piece object. Each piece has a copy of the image texture.
	// It also has a shader attached which allows four masks to be applied which are automatically rotated and placed to
	// hide/show the right part of the image for the piece.

	public float scaleUse = 2.25f;	// scale for pieces once created. 3f will mean they will intersect exactly.

	// Use this for initialization
	void Start () {

		plugs = new Dictionary<int, Texture2D>();		// create the plugs dict
		sockets = new Dictionary<int, Texture2D> ();		// create the sockets dict
			// a plug of ID = 1 will slot into a socket of ID = 1

		loadMasks ();	// load the mask textures from the resources folder
		
	}



	void loadMasks () {

	
		foreach (Texture2D t in Resources.LoadAll("Masks",typeof(Texture2D))) {
			// loop through all the images in the Masks folder

			if (t.name.Contains("edge")) {		

				edge = t;		// this is the edge mask, cache it

			} 

			else {		// must be a socket or plug

				int id;
				string removeConnect = t.name.Replace ("connector", "");
				string removeSocket = removeConnect.Replace ("_socket", "");
				string removePlug = removeConnect.Replace ("_plug", "");

				if (removeConnect == removePlug) {		// is a socket mask

					int.TryParse (removeSocket, out id);	// get the ID from the filename
					sockets.Add (id, t);			// add the mask to the sockets dict

				} else {		// is a plug mask
					
					int.TryParse (removePlug, out id);	// get the ID from the filename
					plugs.Add (id, t);		// add the mask to the plugs dict

				}

				//Debug.Log (t.name + " SOCKETS: " + sockets.Count + " PLUGS: " + plugs.Count + " ID: " + id);

			}
		

		}


	}


	void PaintPiece (Piece p, GameObject go) {

		// gets the right section of the puzzle image for this piece. A 3x3 chunk will be taken.

		float uvWidth = 1.0f / PuzzleController.Instance.cols;
		float uvHeight = 1.0f / PuzzleController.Instance.rows;		// calculate how wide/tall each piece is
			
		Renderer renderer = go.GetComponent<Renderer>();			// get the renderer for this piece
		MaterialPropertyBlock props = new MaterialPropertyBlock();		// set up a material property block
		renderer.GetPropertyBlock(props);			// populate it with this renderer's block
		props.SetColor("_Color", Color.white);		// set the colour to white (could be used to tint)

		Mesh mesh = go.GetComponent<MeshFilter>().mesh;		// get the mesh for the piece quad
		Vector2[] uvs = mesh.uv;	
		mesh.uv2 = uvs;		// store a copy of the original UV in UV2. This is so masks aren't culled.

		uvs[0] = new Vector2((p.col - 1) * uvWidth, (p.row - 1) * uvHeight);
		uvs[3] = new Vector2((p.col - 1) * uvWidth, (p.row + 2) * uvHeight);
		uvs[1] = new Vector2((p.col + 2) * uvWidth, (p.row + 2) * uvHeight);
		uvs[2] = new Vector2((p.col + 2) * uvWidth, (p.row - 1) * uvHeight);	// get the section of texture we want
		mesh.uv = uvs;		// change the UVs to this section

		string[] maskNames = { "_Mask2", "_Mask3", "_Mask", "_Mask4" };	// Mask names set in shader relate to:
		// Right Side, Top Side, Left Side, Bottom Side

		for (int i = 0; i < 4; i++) {		// loop through all the sides of the piece
			
			Side s = p.sides [i];		// get the data relating to this side (i.e it's connectorID and whether socket/plug)
		
			if (s.connectorID == 0) {		// ID is zero, this must be an edge.
				props.SetTexture (maskNames [i], edge);	// set the edge mask texture to this side
				continue;	// skip the next bit so we don't get unwanted mask.
			}	

			if (s.isSocket) {		// this side has another piece slotting into it

				props.SetTexture (maskNames [i], sockets[s.connectorID]);
						// set the right socket mask which will hide this section of image that adjoining piece slots into
			}

			else
				
			{
				props.SetTexture (maskNames [i], plugs[s.connectorID]);
							// show a section of the image relating to the plug which will slot into adjoining piece
			}	


		}

		renderer.SetPropertyBlock(props);	// send the property block back to the renderer



	}


	public void CreatePieceObject (Piece p) {

		// creates the gameObject belonging to the piece

		float xPos = 0.5f -((float)PuzzleController.Instance.cols / 2f) + (p.col) * 1f;
		float yPos = 0.5f -((float)PuzzleController.Instance.rows / 2f) + (p.row) * 1f;
		Vector3 pos = new Vector3 (xPos, yPos, 0f);		// calculate position of piece in game world

		Camera.main.orthographicSize = Mathf.Max (3f, Mathf.Max (PuzzleController.Instance.cols, PuzzleController.Instance.rows) - 3f); 
				// set camera zoom so we can see all the pieces

		GameObject go = Instantiate(piecePrefab, pos, Quaternion.identity);
				// grab the prefab

		PaintPiece (p, go);	// call code to get right part of texture for piece and mask it.

		go.transform.localScale = new Vector3(scaleUse, scaleUse, scaleUse);
		go.transform.SetParent (this.transform);
				// scale and put in pieceController folder

	}

}
