﻿Shader "Custom/PieceMask" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Mask("Mask (A)", 2D) = "white" {}
		_Mask2("Mask (A)", 2D) = "white" {}
		_Mask3("Mask (A)", 2D) = "white" {}
		_Mask4("Mask (A)", 2D) = "white" {}
	}

	SubShader{
		Tags{ "RenderType" = "Transparent" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert alpha
#pragma target 3.0
		sampler2D _MainTex;
		sampler2D _Mask;
		sampler2D _Mask2;
		sampler2D _Mask3;
		sampler2D _Mask4;

		struct Input {
			// UVs that give each piece its part of the full image.
			float2 uv_MainTex;

			// UVs that allow each mask to ignore the previous UVs and retain their full image.
			float2 uv2_Mask;
		};

		fixed4 _Color;
		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;


			float2 uv_Top = IN.uv2_Mask;

			// Rotates the mask to its proper orientation.
			float2 uv_Left = IN.uv2_Mask;
			float2x2 rotationMatrix = float2x2(0, -1, 1, 0);	
			uv_Left.xy = mul(uv_Left.xy, rotationMatrix);
			
			float2 uv_Bot = IN.uv2_Mask;
			rotationMatrix = float2x2(-1, 0, 0, -1);
			uv_Bot.xy = mul(uv_Bot.xy, rotationMatrix);

			float2 uv_Right = IN.uv2_Mask;
			rotationMatrix = float2x2(0, 1, -1, 0);
			uv_Right.xy = mul(uv_Right.xy, rotationMatrix);

			// Puts them all together.
			o.Alpha = c.a * tex2D(_Mask, uv_Left).r * tex2D(_Mask2, uv_Right).r * tex2D(_Mask3, uv_Top).r * tex2D(_Mask4, uv_Bot).r;
		}

		ENDCG
	}

	FallBack "Diffuse"
}